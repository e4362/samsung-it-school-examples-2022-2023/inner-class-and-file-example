package ru.dolbak.personaldata;

import static android.provider.Telephony.Mms.Part.FILENAME;

import static androidx.core.content.PackageManagerCompat.LOG_TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    final String LOG_TAG = "NIKITA";
    final String FILENAME = "MYSECRET.TXT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickWrite(View view) {
        EditText editText1 = findViewById(R.id.editTextTextPersonName);
        EditText editText2 = findViewById(R.id.editTextTextPersonName2);
        EditText editText3 = findViewById(R.id.editTextTextPersonName3);
        EditText editText4 = findViewById(R.id.editTextTextPersonName4);

        class Person{
            String familyName, personalName, fatherName;
            public Person(String name){
                String[] words = name.split(" ");
                familyName = words[0];
                personalName = words[1];
                fatherName = words[2];
            }

            @NonNull
            @Override
            public String toString() {
                return familyName + " " + personalName + " " + fatherName;
            }
        }

        Person person1 = new Person(editText1.getText().toString());
        Person person2 = new Person(editText2.getText().toString());
        Person person3 = new Person(editText3.getText().toString());
        Person person4 = new Person(editText4.getText().toString());

        try {
            BufferedWriter bw = new BufferedWriter(new
                    OutputStreamWriter(openFileOutput (FILENAME, MODE_PRIVATE)));
            String toWrite = person1.toString() + "\n" + person2.toString() + "\n" + person3.toString() + "\n" + person4.toString();
            bw.write(toWrite);
            Log.d(LOG_TAG, "Файл записан");
            bw.close();
            Log.d(LOG_TAG, toWrite);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickRead(View view) {
        try {
// открываем поток для чтения
            BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput (FILENAME)));

            String str = "";
            String text = "";
            while ((str = br.readLine()) != null) {
                Log.d(LOG_TAG, str);
                text += str += "\n";
            }
            TextView textView = findViewById(R.id.textView2);
            textView.setText(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}